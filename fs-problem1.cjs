const fs = require('fs');
const path = require('path');

function createRandomJSONFiles(err, absolutePathOfRandomDirectory, randomNumberOfFiles, callback, callback2) {
    if (err) {
        callback(err);
    } else {
        const filePathsCreated = [];
        let filesCreated = 0;

        for (let index = 1; index <= randomNumberOfFiles; index++) {
            let fileName = `file0${index}.json`;
            let filePath = path.join(absolutePathOfRandomDirectory, fileName);
            let jsonData = JSON.stringify({ random: 'Martial God Asura' });

            fs.writeFile(filePath, jsonData, (err) => {
                if (err) {
                    callback(err);
                } else {
                    filesCreated++;
                    filePathsCreated.push(filePath);

                    if (filesCreated === randomNumberOfFiles) {
                        console.log('Required Conditions Met');
                        callback(null, filePathsCreated, callback2);
                    }
                }
            });
        }
    }
}

function createDirectoryOfRandomJSONFiles(absolutePathOfRandomDirectory, randomNumberOfFiles, callback, callback2, callback3) {
    fs.mkdir(absolutePathOfRandomDirectory, (err) => {
        if (err) {
            callback(err);
        } else {
            callback(null, absolutePathOfRandomDirectory, randomNumberOfFiles, callback2, callback3);
        }
    });
}

function deleteFilesSimultaneously(err, filePathsCreated, callback) {
    if (err) {
        callback(err);
    } else {
        const filePathsDeleted = [];
        let filesDeleted = 0;

        for (let file = 0; file < filePathsCreated.length; file++) {
            fs.unlink(filePathsCreated[file], (err) => {
                if (err) {
                    callback(err);
                } else {
                    filesDeleted++;
                    filePathsDeleted.push(filePathsCreated[file]);

                    if (filesDeleted === filePathsCreated.length) {
                        callback(null, [filePathsCreated, filePathsDeleted]);
                    }
                }
            })
        }
    }
}

function fsProblem1(absolutePathOfRandomDirectory, randomNumberOfFiles) {
    createDirectoryOfRandomJSONFiles(absolutePathOfRandomDirectory, randomNumberOfFiles, createRandomJSONFiles, deleteFilesSimultaneously, function (err, [filePathsCreated, filePathsDeleted]) {
        if (err) {
            console.log('Error:', err.message);
        } else {
            console.log('File Paths Created:', filePathsCreated);
            console.log('File Paths Deleted:', filePathsDeleted);
        }
    });
}

module.exports = fsProblem1;