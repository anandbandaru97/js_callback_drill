function fsProblem2() {
    const fs = require('fs');

    function readFileAsync(fileName, callback) {
        fs.readFile(fileName, 'utf-8', (err, fileData) => {
            if (err) {
                console.error('Error Reading the file:', err);
            } else {
                callback(null, fileData);
            }
        });
    }

    function writeFileAsync(fileName, fileData, callback) {
        fs.writeFile(fileName, fileData, 'utf-8', (err) => {
            if (err) {
                callback(err);
            } else {
                callback(null);
            }
        });
    }

    const convertToUpperCase = (fileContent) => {
        return fileContent.toUpperCase();
    }

    const convertToLowerCase = (fileContent) => {
        const lowerCaseContent = fileContent.toLowerCase();
        return lowerCaseContent.split(/[.!?]\s+/);

    }

    const inputFile = '/home/anand/Desktop/Callbacks/js_callback_drill/lipsum.txt';
    readFileAsync(inputFile, (err, data) => {
        if (err) {
            console.error('Error reading the file:', err);
        } else {
            const upperCaseContent = convertToUpperCase(data);
            const newUpperCaseContentFile = 'uppercase.txt';
            writeFileAsync(newUpperCaseContentFile, upperCaseContent, (err) => {
                if (err) {
                    console.error('Error writing the file:', err);
                } else {
                    console.log('Uppercase content has been written to', newUpperCaseContentFile);

                    const lowerCaseAndSplitContent = convertToLowerCase(upperCaseContent);
                    const newlowerCaseAndSplitContentFile = 'lowerCaseAndSplit.txt';
                    writeFileAsync(newlowerCaseAndSplitContentFile, lowerCaseAndSplitContent.join('\n'), (err) => {
                        if (err) {
                            console.error('Error writing the file:', err);
                        } else {
                            console.log('Lowercase and split content has been written to', newlowerCaseAndSplitContentFile);
                        }

                        const files = [newUpperCaseContentFile, newlowerCaseAndSplitContentFile];
                        const sortedContent = files.map((file) => fs.readFileSync(file, 'utf-8')).sort().join('\n');
                        const newSortedContentFile = 'sorted.txt';
                        writeFileAsync(newSortedContentFile, sortedContent, (err) => {
                            if (err) {
                                console.error('Error writing the file:', err);
                            } else {
                                console.log('Sorted content has been written to', newSortedContentFile);
                            }

                            const filesContent = files.join('\n');
                            const filesName = 'filenames.txt';
                            writeFileAsync(filesName, filesContent, (err) => {
                                if (err) {
                                    console.error('Error writing the files:', err);
                                } else {
                                    console.log('Filenames written to', filesName);

                                    readFileAsync(filesName, (err, data) => {
                                        if (err) {
                                            console.error('Error reading the files:', err);
                                        } else {
                                            const filesToDelete = data.split('\n').filter(Boolean);
                                            filesToDelete.forEach((file) => {
                                                fs.unlink(file, (err) => {
                                                    if (err) {
                                                        console.error('Error deleting the file:', err);
                                                    } else {
                                                        console.log('File deleted:', file);
                                                    }
                                                });
                                            });
                                        }
                                    });
                                }
                            });
                        });
                    });
                }
            });
        }
    });
}

module.exports = fsProblem2;